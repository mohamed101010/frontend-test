# frontend-test

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
### Lints and auto fixes 
npm run lint  --fix

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
